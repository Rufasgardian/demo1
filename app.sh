#!/bin/bash

# Update what is missing and install/update openjdk
sudo apt update --fix-missing -y && sudo apt install openjdk-14-jdk -y

# environment
export MYSQL_PASS="1Q2W3E4r5tzxc@"
export MYSQL_URL="jdbc:mysql://db:3306/petclinic"
export MYSQL_USER="rufat"


# git pull if directory exists, otherwise git clone
function repo {
    Dir=$(basename "$1" .git)
    if [[ -d "$Dir" ]]; then
      cd $Dir
      git pull
    else
      git clone "$1" && cd $Dir
    fi
}


# call the function
repo https://gitlab.com/Rufasgardian/petclinic

chmod +x mvnw

./mvnw clean package
